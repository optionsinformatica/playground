# Install instructions


 In a `.env` file configure the access to your Postgres, for example:

```
DB_ENGINE=django.db.backends.postgresql
DB_HOST=localhost
DB_PORT=5432
DB_USER=playground
DB_PASSWORD=mysecretpassword
```

With Python 3.6.1 run:

```console
$ pip install -r requirements.txt
$ python manage.py migrate
```

---

Vídeos salvos em `O:\Old.01\Cuducos\Django`

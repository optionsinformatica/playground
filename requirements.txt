Django==2.0.1
django-test-without-migrations==0.6
python-decouple==3.1
django-extensions==1.9.9
psycopg2==2.7.3.2

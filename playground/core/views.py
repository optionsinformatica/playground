from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render, resolve_url

from playground.audiometria.forms import AudiometriaForm
from playground.audiometria.models import Audiometria


@login_required
def home(request):
    audiometrias = Audiometria.objects.all()
    context = {'audiometrias': audiometrias}
    return render(request, 'core/home.html', context=context)


@login_required
def new(request):
    if request.method == 'POST':
        form = AudiometriaForm(request.POST)
        if not form.is_valid():
            return render(request, 'core/new.html', context={'form': form})

        Audiometria.objects.create(**form.cleaned_data)
        return redirect(resolve_url('home'))

    form = AudiometriaForm()
    return render(request, 'core/new.html', context={'form': form})


@login_required
def edit(request, pk):
    audiometria = Audiometria.objects.get(pk=pk)

    if request.method == 'POST':
        form = AudiometriaForm(request.POST)
        if not form.is_valid():
            return render(request, 'core/new.html', context={'form': form})

        audiometria.aod500 = form.cleaned_data['aod500']
        audiometria.aod1000 = form.cleaned_data['aod1000']
        audiometria.save()
        return redirect(resolve_url('home'))

    data = dict(aod500=audiometria.aod500, aod1000=audiometria.aod1000)
    form = AudiometriaForm(data)
    return render(request, 'core/new.html', context={'form': form})

from django.contrib.auth import get_user_model
from django.shortcuts import resolve_url
from django.test import TestCase

from playground.audiometria.models import Audiometria
from playground.core.models import Colaborador


User = get_user_model()


class TestAuthentication(TestCase):

    def test_status_without_login(self):
        new = resolve_url('new')
        login = resolve_url('login')
        response = self.client.get(new)
        self.assertRedirects(response, f'{login}?next={new}')

    def test_status_with_login(self):
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')
        response = self.client.get(resolve_url('new'))
        self.assertEqual(200, response.status_code)


class TestGet(TestCase):

    def setUp(self):
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')

    def test_content(self):
        response = self.client.get(resolve_url('new'))
        expected = (
            'name="aod500"',
            'name="aod1000"',
            'Salvar</button>'
        )
        for content in expected:
            with self.subTest():
                self.assertContains(response, content)


class TestPost(TestCase):

    def setUp(self):
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')

    def test_valid_post_creates_data_in_the_database(self):
        colaborador = Colaborador.objects.create(name='Fulano')
        data = {'aod500': '5', 'aod1000': '40', 'colaborador': colaborador.pk}
        response = self.client.post(resolve_url('new'), data)
        self.assertEqual(1, Audiometria.objects.count())

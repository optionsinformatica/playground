from django.contrib.auth import get_user_model
from django.shortcuts import resolve_url
from django.test import TestCase

from playground.audiometria.models import Audiometria
from playground.core.models import Colaborador


User = get_user_model()


class TestEditCase(TestCase):

    def setUp(self):
        colaborador = Colaborador.objects.create(name='Fulano')
        data = {'aod500': '5', 'aod1000': '40', 'colaborador': colaborador}
        audiometria = Audiometria.objects.create(**data)
        self.url = resolve_url('edit', audiometria.pk)
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')


class TestAuthentication(TestEditCase):

    def test_status_without_login(self):
        self.client.logout()
        login = resolve_url('login')
        response = self.client.get(self.url)
        self.assertRedirects(response, f'{login}?next={self.url}')

    def test_status_with_login(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)


class TestGet(TestEditCase):

    def test_content(self):
        response = self.client.get(self.url)
        expected = (
            'name="aod500"',
            'value="5"',
            'name="aod1000"',
            'value="40"',
            'Salvar</button>'
        )
        for content in expected:
            with self.subTest():
                self.assertContains(response, content)


class TestPost(TestEditCase):

    def test_valid_post_edits_data_in_the_database(self):
        audiometria = Audiometria.objects.first()
        data = {
            'aod500': '100',
            'aod1000': '20',
            'colaborador': audiometria.colaborador.pk
        }
        response = self.client.post(self.url, data)

        audiometria = Audiometria.objects.first()
        self.assertEqual(100, audiometria.aod500)
        self.assertEqual(20, audiometria.aod1000)
        self.assertEqual(1, Audiometria.objects.count())

from django.contrib.auth import get_user_model
from django.test import TestCase


User = get_user_model()


class TestCreate(TestCase):

    def test_create(self):
        user = User.objects.create_user('options', password='1234')
        user.cpf = '11111111111'
        user.save()

        user = User.objects.first()
        self.assertEqual(user.cpf, '11111111111')

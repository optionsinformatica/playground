import time

from django.test import TestCase
from django.utils.timezone import now

from playground.core.models import Colaborador


class TestCreate(TestCase):

    def test_create(self):
        self.assertEqual(0, Colaborador.objects.count())
        Colaborador.objects.create(
            name='Fulano',
            nascimento=now()
        )
        self.assertEqual(1, Colaborador.objects.count())

    def test_auto_fields(self):
        colaborador = Colaborador.objects.create(
            name='Fulano',
            nascimento=now()
        )
        self.assertEqual(
            colaborador.criado_em.strftime('%d/%m/%Y %H:%M:%S'),
            colaborador.alterado_em.strftime('%d/%m/%Y %H:%M:%S')
        )

        time.sleep(1)
        colaborador.name = 'Ciclano'
        colaborador.save()

        self.assertNotEqual(
            colaborador.criado_em.strftime('%d/%m/%Y %H:%M:%S'),
            colaborador.alterado_em.strftime('%d/%m/%Y %H:%M:%S')
        )

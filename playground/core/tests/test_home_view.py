from django.contrib.auth import get_user_model
from django.shortcuts import resolve_url
from django.test import TestCase

from playground.audiometria.models import Audiometria
from playground.core.models import Colaborador


User = get_user_model()


class TestAuthentication(TestCase):

    def test_status_without_login(self):
        home = resolve_url('home')
        login = resolve_url('login')
        response = self.client.get(home)
        self.assertRedirects(response, f'{login}?next={home}')

    def test_status_with_login(self):
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')
        response = self.client.get(resolve_url('home'))
        self.assertEqual(200, response.status_code)


class TestGet(TestCase):

    def setUp(self):
        User.objects.create_user('options', password='1234')
        self.client.login(username='options', password='1234')

    def test_content(self):
        Audiometria.objects.create(
            aod500=5,
            aod1000=100,
            colaborador=Colaborador.objects.create(name='Fulano')
        )
        response = self.client.get(resolve_url('home'))
        expected = (
            'Audiometria (Fulano)',
            'Visualizar',
            'Nova audiometria'
        )
        for content in expected:
            with self.subTest():
                self.assertContains(response, content)

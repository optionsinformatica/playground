from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    cpf = models.CharField('cpf', max_length=11)


class Colaborador(models.Model):
    name = models.CharField('nome', max_length=128)
    nascimento = models.DateField('data de nascimento', null=True)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)
    alterado_em = models.DateTimeField('alterado em', auto_now=True)
    empresa = models.OneToOneField('Empresa', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Empresa(models.Model):
    name = models.CharField('nome', max_length=128)

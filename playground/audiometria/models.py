from django.db import models


class Audiometria(models.Model):

    NDG = 'N'
    AUS = 'A'
    OK = 'P'
    CHOICES = (
        (NDG, 'Não digitado'),
        (AUS, 'Ausente'),
        (OK, 'Preenchido')
    )

    aod500 = models.IntegerField('AOD 500', null=True)
    aod500_flag = models.CharField(max_length=1, choices=CHOICES, default=NDG)

    aod1000 = models.IntegerField('AOD 1000', null=True)
    aod1000_flag = models.CharField(max_length=1, choices=CHOICES, default=NDG)

    colaborador = models.ForeignKey('core.Colaborador', on_delete=models.CASCADE)

    def get_value(self, field):
        value = getattr(self, field)
        if not value:
            return getattr(self, f'{field}_flag')

        return value

    def save(self, *args, **kwargs):

        # fix flags to OK is values are present
        for field in ('aod500', 'aod1000'):
            if getattr(self, field):
                setattr(self, f'{field}_flag', self.OK)

        super(Audiometria, self).save(*args, **kwargs)

    def __str__(self):
        return f'Audiometria ({self.colaborador})'


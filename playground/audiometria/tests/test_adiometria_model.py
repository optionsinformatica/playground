from django.test import TestCase

from playground.audiometria.models import Audiometria
from playground.core.models import Colaborador


class TestCreate(TestCase):

    def setUp(self):
        self.colaborador = Colaborador.objects.create(name='Fulano')

    def test_create(self):
        self.assertEqual(0, Audiometria.objects.count())
        Audiometria.objects.create(
            aod500=5,
            aod500_flag=Audiometria.OK,
            aod1000=15,
            aod1000_flag=Audiometria.OK,
            colaborador=self.colaborador
        )
        self.assertEqual(1, Audiometria.objects.count())

    def test_get_existent_aod_500(self):
        audio = Audiometria.objects.create(
            aod500=5,
            aod500_flag=Audiometria.OK,
            colaborador=self.colaborador
        )
        self.assertEqual(5, audio.get_value('aod500'))

    def test_get_non_existent_aod_500(self):
        audio = Audiometria.objects.create(
            aod500_flag=Audiometria.NDG,
            colaborador=self.colaborador
        )
        self.assertEqual('N', audio.get_value('aod500'))

    def test_input_changes_flag(self):
        audio = Audiometria.objects.create(aod500=5, colaborador=self.colaborador)
        self.assertEqual(Audiometria.OK, audio.aod500_flag)

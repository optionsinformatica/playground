from django.test import TestCase

from playground.audiometria.forms import AudiometriaForm
from playground.core.models import Colaborador


class TestValidation(TestCase):

    def setUp(self):
        self.colaborador = Colaborador.objects.create(name='Fulano')

    def test_valid_data(self):
        form = AudiometriaForm({'aod500': '5', 'colaborador': self.colaborador.pk})
        self.assertTrue(form.is_valid())

    def test_invalid_data(self):
        form = AudiometriaForm({'aod500': '2', 'colaborador': self.colaborador.pk})
        self.assertFalse(form.is_valid())

    def test_audiometria_sem_colaborador_eh_invalida(self):
        form = AudiometriaForm({'aod500': '5'})
        self.assertFalse(form.is_valid())

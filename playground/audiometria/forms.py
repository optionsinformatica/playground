from django.core.exceptions import ValidationError
from django.forms import IntegerField, Form, ModelChoiceField

from playground.core.models import Colaborador


class FrequencyField(IntegerField):

    def __init__(self, *args, **kwargs):
        kwargs['max_value'] = 130
        kwargs['min_value'] = -10
        kwargs['required'] = False
        super().__init__(*args, **kwargs)
        self.validators.append(self.validate_frequency)

    def validate_frequency(self, value):
        if value not in range(-10, 131, 5):
            raise ValidationError('Frequência inválida', 'invalid_frequency')


class AudiometriaForm(Form):

    aod500 = FrequencyField(label='AOD 500')
    aod1000 = FrequencyField(label='AOD 1000')
    colaborador = ModelChoiceField(queryset=Colaborador.objects.all(), empty_label=None)

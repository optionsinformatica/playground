"""playground URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from playground.core.views import edit, home, new

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^new/$', new, name='new'),
    url(r'^edit/(?P<pk>\d+)/$', edit, name='edit'),
    url(r'^login/$', admin.site.login, name='login'),
    url(r'^admin/', admin.site.urls)
]
